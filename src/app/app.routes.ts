import { Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ItemsComponent } from './components/items/items.component';
import { AuthGuardService } from './services/auth-guard.service';

export const routes: Routes = [
    { path: '', component: LoginComponent },
    { path: '', canActivate: [AuthGuardService], children: [
        
        { path: 'items', component: ItemsComponent }
        
    ] },
    { path: '**', component: NotFoundComponent }
];  