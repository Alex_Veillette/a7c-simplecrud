import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-item-create',
  templateUrl: './item-create.component.html',
  styleUrls: ['./item-create.component.css']
})
export class ItemCreateComponent implements OnInit {
  itemForm: FormGroup;
  nameControl: FormControl;

  constructor(formBuilder: FormBuilder) {
    this.nameControl = new FormControl('', Validators.required);

    this.itemForm = formBuilder.group(
      { 
        name: this.nameControl,
        description: new FormControl('', [Validators.required])
      }
    );
  }

  create() {
    const itemValues = this.itemForm.value;
    
    //itemValues.name
  }

  ngOnInit() {
  }

}
