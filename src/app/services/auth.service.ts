import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUser: User;
  
  constructor() {
    this.currentUser = JSON.parse(localStorage.getItem("CU"));
  }

  get getCurrentUser(): User {
    return this.currentUser;
  }

  login(email: string, password: string): User {
    if (email === 'a@a' && password === 'a') {
      this.currentUser = new User(email);

      localStorage.setItem("CU", JSON.stringify(this.currentUser));

      return this.currentUser;
    }
    else {
      return null;
    }
  }

  logout() {
    this.currentUser = null;
    localStorage.setItem("CU", null);
  }

}
